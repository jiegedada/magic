import Vue from 'vue'
import VueRouter from 'vue-router'
const Index = () => import('@/components/Index.vue')
const About = () => import('@/components/About.vue')
const Count = () => import('@/components/Count.vue')
const Partners = () => import('@/components/Partners.vue')
const Contact = () => import('@/components/Contact.vue')
const Service = () => import('@/components/Service.vue')

const Builder = () => import('@/components/Count/Builder.vue')
const Programming = () => import('@/components/Count/Programming.vue')
const Scratch = () => import('@/components/Count/Scratch.vue')
const Python = () => import('@/components/Count/Python.vue')
const Competition = () => import('@/components/Count/Competition.vue')

Vue.use(VueRouter)
const routerPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location) {
  return routerPush.call(this, location).catch(error => error)
}

const routes = [
  {
    path: '/',
    name: 'Index',
    component: Index
  },
  {
    path: '/index',
    name: 'Index',
    component: Index
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/count',
    component: Count,
    children: [
      {
        path: '/build',
        component: Builder
      },
      {
        path: '/count',
        redirect: '/build'
      },
      {
        path: '/programming',
        component: Programming
      },
      {
        path: '/scratch',
        component: Scratch
      },
      {
        path: '/python',
        component: Python
      },
      {
        path: '/competition',
        component: Competition
      }
    ]
  },
  {
    path: '/partners',
    name: 'Partners',
    component: Partners
  },
  {
    path: '/contact',
    name: 'Contact',
    component: Contact
  },
  {
    path: '/service',
    name: 'Service',
    component: Service
  }
]

const router = new VueRouter({
  routes
})

export default router
