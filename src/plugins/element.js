import Vue from 'vue'
import {
  Menu,
  MenuItem,
  Submenu,
  Row,
  Col,
  Container,
  Input,
  Button,
  Message,
  Breadcrumb,
  BreadcrumbItem,
  Form,
  FormItem,
  Header,
  Main,
  Aside,
  Card,
  TableColumn,
  Switch,
  Tooltip,
  Pagination,
  Dialog,
  Tag,
  Tree,
  Carousel,
  CarouselItem
} from 'element-ui'

Vue.use(CarouselItem)
Vue.use(Carousel)
Vue.use(Menu)
Vue.use(MenuItem)
Vue.use(Submenu)
Vue.use(Row)
Vue.use(Col)
Vue.use(Container)
Vue.use(Input)
Vue.use(Button)
Vue.use(Breadcrumb)
Vue.use(BreadcrumbItem)
Vue.use(Tree)
Vue.use(Header)
Vue.use(Tag)
Vue.use(Dialog)
Vue.use(Pagination)
Vue.use(Tooltip)
Vue.use(Switch)
Vue.use(TableColumn)
Vue.use(Card)
Vue.use(Submenu)
Vue.use(Aside)
Vue.use(Main)
Vue.use(Form)
Vue.use(FormItem)
Vue.prototype.$message = Message
